Import dump (run inside db container under postgres user):

```bash
createdb mt_stats
psql mt_stats < /tmp/data_import/mt_stats.sql
```
